Drupal + CiviCRM Base
=====================

This repo contains all the open-source projects we like to start with when developing a CiviCRM solution atop the Drupal 7 CMS. It is provided as is. Although we'll endeavor to keep this codebase updated with the latest security patches, please ensure you check for and install any critical updates before releasing into the wild.

* Drupal 7 core (https://www.drupal.org/)
* CiviCRM + localisation (https://civicrm.org/)
* Drupal contrib modules:
  - admin_menu
  - adminimal_admin_menu
  - civicrm
  - civicrm_clear_all_caches
  - civicrm_cron
  - css_injector
  - ctools
  - devel
  - jquery_update
  - libraries
  - masquerade
  - module_filter
  - options_element
  - views
  - webform
  - webform_civicrm
* Drupal contrib themes:
  - adminimal_theme
  - civi_bartik
